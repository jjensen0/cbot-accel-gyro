/*****************************************************************************************
 *	Copyright (C) 2014 The Board of Regents of the University of Nebraska.				 *
 *	All rights reserved.																 *
 *																						 *
 *	Redistribution and use in source and binary forms, with or without modification,	 *
 *	are permitted provided that the following conditions are met:						 *
 *																						 *
 *	1. Redistributions of source code must retain the above copyright notice, this list  *
 *		of conditions and the following disclaimer.										 *
 *																						 *
 *	2. Redistributions in binary form must reproduce the above copyright notice, this 	 *
 *		list of conditions and the following disclaimer in the documentation and/or 	 *
 *		other materials provided with the distribution.									 *
 *																						 *
 *	3. Neither the name of the copyright holder nor the names of its contributors may be *
 *		used to endorse or promote products derived from this software without specific  *
 *		prior written permission.														 *
 *																						 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY  *
 *	EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES *
 *	OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT  *
 *	SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 		 *
 *	INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED *
 *	TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 	 *
 *	BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 	 *
 *	CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN   *
 *	ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH  *
 *	DAMAGE.																				 *
 *																						 *
 *****************************************************************************************/

// Desc: Interfacing the CEENBoT with the MPU-6050, getting data and displaying the data
//		 to the computer via UART.
// Auth: Cannon Biggs
// CoAuth: TJ Hoke, Kevin Dethloffs-Moreno

/** Accelerometer Specifications: **
 *
 * __PARAMETER__				__CONDITIONS__		__MIN__		__TYP__		__MAX__		__UNITS__
 * ACCELEROMETER SENSITIVITY
 * Full-Scale Range				AFS_SEL = 0						+/- 2						g
 * 								AFS_SEL = 1						+/- 4						g
 * 								AFS_SEL = 2						+/- 8						g
 * 								AFS_SEL = 3						+/- 16						g
 *
 * ADC Word Length				Output in 2's					  16			   		   bits
 * 							  	complement format
 *
 * Sensitivity Scale Factor		AFS_SEL = 0						16384					  LSB/g
 * 								AFS_SEL = 1						8192					  LSB/g
 * 								AFS_SEL = 2						4096					  LSB/g
 * 								AFS_SEL = 3						2048					  LSB/g
 *
 * LOW PASS FILTER RESPONSE		Programmable Range	   5					  260		   Hz
 * OUTPUT DATA RATE				Programmable Range	   4					  1,000		   Hz
 */

/** Gyroscope Specifications: **
 *
 * __PARAMETER__				__CONDITIONS__		__MIN__		__TYP__		__MAX__		__UNITS__
 * ACCELEROMETER SENSITIVITY
 * Full-Scale Range				FS_SEL = 0						+/- 250					   °/s
 * 								FS_SEL = 1						+/- 500					   °/s
 * 								FS_SEL = 2						+/- 1000				   °/s
 * 								FS_SEL = 3						+/- 2000				   °/s
 *
 * ADC Word Length				Output in 2's					  16			   		   bits
 * 							  	complement format
 *
 * Sensitivity Scale Factor		FS_SEL = 0						 131					  LSB/g
 * 								FS_SEL = 1						 65.5					  LSB/g
 * 								FS_SEL = 2						 32.8					  LSB/g
 * 								FS_SEL = 3						 16.4					  LSB/g
 *
 * LOW PASS FILTER RESPONSE		Programmable Range	   5					  256		   Hz
 * OUTPUT DATA RATE				Programmable Range	   4					  8,000		   Hz
 */

#include "capi324v221.h"
#include <math.h>
#include <stdbool.h>

#ifndef DEBUG
#define DEBUG

/* 57.296 radians in one degree */
#define Radian 	57.2957795
#define PI 		3.14159265

#define MPU6050_ADDRESS_AD0_LOW		0x68
#define MPU6050_ADDRESS_AD0_HIGH	0x69

// set default configuration
#define MPU6050_ADDR	MPU6050_ADDRESS_AD0_LOW

// MPU6050 register addresses & bits
#define MPU6050_XG_OFFS_TC       	0x00 //[7] PWR_MODE, [6:1] XG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_YG_OFFS_TC       	0x01 //[7] PWR_MODE, [6:1] YG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_ZG_OFFS_TC       	0x02 //[7] PWR_MODE, [6:1] ZG_OFFS_TC, [0] OTP_BNK_VLD
#define MPU6050_X_FINE_GAIN      	0x03 //[7:0] X_FINE_GAIN
#define MPU6050_Y_FINE_GAIN      	0x04 //[7:0] Y_FINE_GAIN
#define MPU6050_Z_FINE_GAIN      	0x05 //[7:0] Z_FINE_GAIN
#define MPU6050_XA_OFFS_H        	0x06 //[15:0] XA_OFFS
#define MPU6050_XA_OFFS_L_TC     	0x07
#define MPU6050_YA_OFFS_H        	0x08 //[15:0] YA_OFFS
#define MPU6050_YA_OFFS_L_TC     	0x09
#define MPU6050_ZA_OFFS_H        	0x0A //[15:0] ZA_OFFS
#define MPU6050_ZA_OFFS_L_TC     	0x0B
#define MPU6050_XG_OFFS_USRH     	0x13 //[15:0] XG_OFFS_USR
#define MPU6050_XG_OFFS_USRL     	0x14
#define MPU6050_YG_OFFS_USRH     	0x15 //[15:0] YG_OFFS_USR
#define MPU6050_YG_OFFS_USRL     	0x16
#define MPU6050_ZG_OFFS_USRH     	0x17 //[15:0] ZG_OFFS_USR
#define MPU6050_ZG_OFFS_USRL     	0x18
#define MPU6050_SMPLRT_DIV       	0x19
#define MPU6050_CONFIG           	0x1A
#define MPU6050_GYRO_CONFIG      	0x1B
#define MPU6050_ACCEL_CONFIG     	0x1C
#define MPU6050_FF_THR           	0x1D
#define MPU6050_FF_DUR           	0x1E
#define MPU6050_MOT_THR          	0x1F
#define MPU6050_MOT_DUR          	0x20
#define MPU6050_ZRMOT_THR        	0x21
#define MPU6050_ZRMOT_DUR        	0x22
#define MPU6050_FIFO_EN          	0x23
#define MPU6050_ACCEL_XOUT_H     	0x3B
#define MPU6050_ACCEL_XOUT_L     	0x3C
#define MPU6050_ACCEL_YOUT_H     	0x3D
#define MPU6050_ACCEL_YOUT_L     	0x3E
#define MPU6050_ACCEL_ZOUT_H     	0x3F
#define MPU6050_ACCEL_ZOUT_L     	0x40
#define MPU6050_TEMP_OUT_H       	0x41
#define MPU6050_TEMP_OUT_L       	0x42
#define MPU6050_GYRO_XOUT_H      	0x43
#define MPU6050_GYRO_XOUT_L      	0x44
#define MPU6050_GYRO_YOUT_H      	0x45
#define MPU6050_GYRO_YOUT_L      	0x46
#define MPU6050_GYRO_ZOUT_H      	0x47
#define MPU6050_GYRO_ZOUT_L      	0x48
#define MPU6050_MOT_DETECT_STATUS   0x61
#define MPU6050_SIGNAL_PATH_RESET   0x68		// what is this?
#define MPU6050_MOT_DETECT_CTRL     0x69
#define MPU6050_USER_CTRL       	0x6A
#define MPU6050_PWR_MGMT_1			0x6B
#define	MPU6050_PWR_MGMT_2			0x6C
#define MPU6050_BANK_SEL         	0x6D
#define MPU6050_MEM_START_ADDR   	0x6E
#define MPU6050_MEM_R_W          	0x6F
#define MPU6050_DMP_CFG_1        	0x70
#define MPU6050_DMP_CFG_2        	0x71
#define MPU6050_FIFO_COUNTH      	0x72
#define MPU6050_FIFO_COUNTL      	0x73
#define MPU6050_FIFO_R_W         	0x74
#define MPU6050_WHO_AM_I			0x75

#define MPU6050_GCONFIG_FS_SEL_BIT      4
#define MPU6050_GCONFIG_FS_SEL_LENGTH   2

#define MPU6050_GYRO_FS_250         0x00
#define MPU6050_GYRO_FS_500         0x01
#define MPU6050_GYRO_FS_1000        0x02
#define MPU6050_GYRO_FS_2000        0x03

#define MPU6050_ACONFIG_XA_ST_BIT           7
#define MPU6050_ACONFIG_YA_ST_BIT           6
#define MPU6050_ACONFIG_ZA_ST_BIT           5
#define MPU6050_ACONFIG_AFS_SEL_BIT         4
#define MPU6050_ACONFIG_AFS_SEL_LENGTH      2
#define MPU6050_ACONFIG_ACCEL_HPF_BIT       2
#define MPU6050_ACONFIG_ACCEL_HPF_LENGTH    3

#define MPU6050_ACCEL_FS_2          0x00
#define MPU6050_ACCEL_FS_4          0x01
#define MPU6050_ACCEL_FS_8          0x02
#define MPU6050_ACCEL_FS_16         0x03

// PWR_MGMT_1 bits
#define DEVICE_RESET   7
#define SLEEP          6
#define CYCLE          5
//		-			   4
#define TEMP_DIS       3
#define CLKSEL         2

// CLKSEL bits in PWR_MGMT_1 register
// 	For further infor on clock source, see Product Specs document
#define CLKSEL_INT8M          0		// Internal 8MHz oscillator
#define CLKSEL_PLL_XGYRO      1
#define CLKSEL_PLL_YGYRO      2
#define CLKSEL_PLL_ZGYRO      3
#define CLKSEL_PLL_EXT32K     4
#define CLKSEL_PLL_EXT19M     5
#define CLKSEL_KEEP_RESET     7		// Stops the clock & keeps the timing
									// generator in reset

#define AccelScale2 	16384
#define GyroScale250 	131
#define GyroScale 		GyroScale250
#define AccelScale 		AccelScale2

uint8_t devAddr;

typedef struct Motion6Data_Raw
{
	struct
	{
		int x;
		int y;
		int z;
	} accel;

	struct
	{
		int x;
		int y;
		int z;
	} gyro;

} Motion6Raw;

typedef struct Motion6Data_Scaled
{
	struct
	{
		float x;
		float y;
		float z;

	}accel;

	struct
	{
		float x;
		float y;
		float z;
	} gyro;

} Motion6Scaled;



typedef enum ORIENTATION
{
	NEGATIVE,		// = 0; corresponds to STEPPER_FWD
	POSITIVE,		// = 1; corresponds to STEPPER_REV
} orientation;

orientation accel_dir;
orientation gyro_dir;

/* READING MPU-6050 6-AXIS DATA */
unsigned char x_accel_h;
unsigned char x_accel_l;
unsigned char y_accel_h;
unsigned char y_accel_l;
unsigned char z_accel_h;
unsigned char z_accel_l;
unsigned char t_h;
unsigned char t_l;
unsigned char x_gyro_h;
unsigned char x_gyro_l;
unsigned char y_gyro_h;
unsigned char y_gyro_l;
unsigned char z_gyro_h;
unsigned char z_gyro_l;

Motion6Raw raw;
Motion6Scaled scaled;

double atan_param, atan_result;

/* P_CONTROL PARAMS */
//---defines
#define TargetAngle	90			// When battery is on motors, angle of balance is between 81°-86°
#define dT			0.01			// gathering data every 10 ms

//---current information
float curr_angle;
float curr_gyro;			// current gyro is the current y value from the gyroscope
							// curr_gyro is also the error (because set point is 0)
float prev_gyro = 0;
float error;
float prev_error = 0;


//---control variables

//---control gains

//---actual output to motors
STEPPER_DIR output_dir;
unsigned short int output_steps;
unsigned short int output_speed;

/* TIMERSVC */
int intCount;

// ---------------------------------- prototypes ---------------------------------- //
void accelgyro_initialize();
int8_t accelgyro_testConnection();
void accelgyro_write( unsigned char registerAddress, unsigned char value );
int8_t* accelgyro_read( unsigned char registerAddress, int numBytes );
void accelgyro_getMotion6Raw();
void accelgyro_getMotion6Scaled();
void _getBotStatus();

void updateTimerSetup();
CBOT_ISR( getData );

// ---------------------------------- CBOT ISRs ----------------------------------- //
CBOT_ISR( getData )
{
	PORTA |= (1<<PA4);
	_getBotStatus();
	PORTA &= ~(1<<PA4);
}

// ---------------------------------- CBOT main ----------------------------------- //
void CBOT_main()
{

	// We will read data from the MPU-6050 via an Interrupt Service Routine.
	// This allows the CEENBoT to do other things, instead of waiting to receive
	// data from the external sensor.  This interrupt could have been set up a
	// number of different ways.  One way would be to utilize the Data Ready Interrupt
	// from the MPU-6050, but we found that this sends an interrupt too often.
	// We calculated that we would only be able to process the interrupt
	ISR_attach(ISR_TIMER1_COMPA_VECT, getData);

	// configure UART
	UART_open( UART_UART0 );
	UART_configure( UART_UART0, UART_8DBITS, UART_1SBIT, UART_NO_PARITY, 230400);
	UART_set_TX_state( UART_UART0, UART_ENABLE );

	I2C_open();
	I2C_pullup_enable();

	STEPPER_open();
	STEPPER_set_pwr_mode( STEPPER_PWR_HIGH );
	STEPPER_set_accel( STEPPER_BOTH, 1000 );

	DELAY_ms( 100 );

	// initialize the device
	UART0_printf("Initializing I2C device...\n");
	accelgyro_initialize();

	// verify connection
	UART0_printf("Testing device connection...\n");

	uint8_t device_id = accelgyro_testConnection();
	UART0_printf("Device ID: 0x%02x\n", device_id);

	DELAY_ms( 10 );

	updateTimerSetup();

	while(1)
	{
		UART0_printf("%6.2f�\t%6.2f�/s\n", curr_gyro, curr_angle);
	}

} // end CBOT_main()

void updateTimerSetup()
{
	// Initialize timer 1 interrupt for setting target position
	TCCR1A = (1<<COM1B1) | (1<<WGM11) | (1<<WGM10); 		// WGM11=1,WGM10=1: count to OCR1A instead of 0xffff
	TCCR1B = (1<<WGM13) | (1<<WGM12) |(1<<CS11)| (1<<CS10);	// CS10=1: use clk I/O with 1:64 prescaler

	OCR1AH = 0x0C;											// count up to 0x04e2 (1250 for 16kHz PWM)
	OCR1AL = 0x35; 											// count up to 0x04e2 (1250 for 16kHz PWM)

	OCR1BH = 0x01; 											// % duty cycle
	OCR1BL = 0x00;

	TIMSK1 |= ( 1 << OCIE1A );								// OCIE1A=1: enable Timer/Counter1 Output Compare Match A Interrupt

	sei();													// set global interrupts
}

/* http://www.cplusplus.com/reference/cmath/atan/ */
// get current angle wrt ground and get current rate of change of angle (y axis on gyro)

/* variables important:
 * const float dt = 0.01; // 10ms
 */
void _getBotStatus()
{
	// get raw data
	accelgyro_getMotion6Raw();

	// scale the data
	scaled.accel.x = (float) raw.accel.x / AccelScale;
	scaled.accel.z = (float) raw.accel.z / AccelScale;
	scaled.gyro.y =  (float) raw.gyro.y / GyroScale;

	atan_param = (double) scaled.accel.x / scaled.accel.z;

	/* Find angle of incline with accel data using arc tangent function.
	 * atan() returns a value with a range of [-PI/2, PI/2] in radians.
	 * The following gets a result in degrees: 							*/
	atan_result = atan(atan_param) * 180/PI;

	if (atan_result < 0)
		atan_result += 180;

	/* modify global vars accel_angle and gyro_y */
	curr_angle = (float) atan_result;

	prev_gyro = curr_gyro;
	curr_gyro = scaled.gyro.y;

} // end _getBotStatus()

/** accelgyro_initialize():
 * Power on and prepare for general usage.
 * This will activate the device and take it out of sleep mode (which must be done
 * after start-up). This function also sets both the accelerometer and the gyroscope
 * to their most sensitive settings, namely +/- 2g and +/- 250 degrees/sec, and sets
 * the clock source to use the X Gyro for reference, which is slightly better than
 * the default internal clock source.
 *
 ** On setting clock source setting:
 * An internal 8MHz oscillator, gyroscope based clock, or external sources can
 * be selected as the MPU-60X0 clock source. When the internal 8 MHz oscillator
 * or an external source is chosen as the clock source, the MPU-60X0 can operate
 * in low power modes with the gyroscopes disabled.
 *
 * Upon power up, the MPU-60X0 clock source defaults to the internal oscillator.
 * However, it is highly recommended that the device be configured to use one of
 * the gyroscopes (or an external clock source) as the clock reference for
 * improved stability. The clock source can be selected according to the following table:
 *
 *
 * CLK_SEL | Clock Source
 * --------+--------------------------------------
 * 0       | Internal 8 MHz oscillator
 * 1       | PLL with X Gyro reference
 * 2       | PLL with Y Gyro reference
 * 3       | PLL with Z Gyro reference
 * 4       | PLL with external 32.768kHz reference
 * 5       | PLL with external 19.2MHz reference
 * 6       | Reserved
 * 7       | Stops the clock and keeps the timing generator in reset
 *
 */
void accelgyro_initialize()
{
	char data;
	// Set clock source setting to Phase-Locked Loop with X axis gyroscope
	// as reference.
	data = (0x00)|(1 << TEMP_DIS)|(CLKSEL_PLL_XGYRO);
	accelgyro_write( MPU6050_PWR_MGMT_1, data );

	// set full-scale gyroscope range
	data = (0x00)|(MPU6050_GYRO_FS_250 << MPU6050_GCONFIG_FS_SEL_BIT);
	accelgyro_write( MPU6050_GYRO_CONFIG, data );

	// set full-scale accelerometer range
	data = (0x00)|(MPU6050_ACCEL_FS_2 << MPU6050_ACONFIG_AFS_SEL_BIT);
	accelgyro_write( MPU6050_ACCEL_CONFIG, data );

	// Set on-chip Low Pass Filter configuration to max. Will still receive
	// data at 1kHz (if using data ready interrupt)
	accelgyro_write( MPU6050_CONFIG, 0x06);

} // end accelgyro_initialize()


void accelgyro_getMotion6Scaled()
{
	accelgyro_getMotion6Raw();

	scaled.accel.x = (float) raw.accel.x / AccelScale;
	scaled.accel.y = (float) raw.accel.y / AccelScale;
	scaled.accel.z = (float) raw.accel.z / AccelScale;

	scaled.gyro.x = (float) raw.gyro.x / GyroScale;
	scaled.gyro.y = (float) raw.gyro.y / GyroScale;
	scaled.gyro.z = (float) raw.gyro.z / GyroScale;

} // end accelgyro_getMotion6Scaled()

/** Get raw 6-axis motion sensor readings from accelerometer and gyroscope.
 * Gets all currently available motion sensor values.
 */
void accelgyro_getMotion6Raw()
{

    if( I2C_MSTR_start( 0x68, I2C_MODE_MT ) == I2C_STAT_OK )
    	{
    		// will start reading from register ACCEL_XOUT_H
    		I2C_MSTR_send( 0x3B );
    	}
    	if ( I2C_MSTR_start( 0x68, I2C_MODE_MR ) == I2C_STAT_OK )
    	{
    		I2C_MSTR_get( &x_accel_h, TRUE );
    		I2C_MSTR_get( &x_accel_l, TRUE );
    		I2C_MSTR_get( &y_accel_h, TRUE );
    		I2C_MSTR_get( &y_accel_l, TRUE );
    		I2C_MSTR_get( &z_accel_h, TRUE );
    		I2C_MSTR_get( &z_accel_l, TRUE );
    		I2C_MSTR_get( &t_h, TRUE );
    		I2C_MSTR_get( &t_l, TRUE );
    		I2C_MSTR_get( &x_gyro_h, TRUE );
    		I2C_MSTR_get( &x_gyro_l, TRUE );
    		I2C_MSTR_get( &y_gyro_h, TRUE );
    		I2C_MSTR_get( &y_gyro_l, TRUE );
    		I2C_MSTR_get( &z_gyro_h, TRUE );
    		I2C_MSTR_get( &z_gyro_l, FALSE );
    		I2C_MSTR_stop();
    	}
    	// TODO calculate offsets / biases

	raw.accel.x = ((x_accel_h << 8) | x_accel_l);

	raw.accel.y = ((y_accel_h << 8) | y_accel_l);

	raw.accel.z = ((z_accel_h << 8) | z_accel_l);

//  temperature = ((t_h << 8) | t_l);

	raw.gyro.x = ((x_gyro_h << 8) | x_gyro_l );

	raw.gyro.y = ((y_gyro_h << 8) | y_gyro_l );
	raw.gyro.y += 283;

	raw.gyro.z = ((z_gyro_h << 8) | z_gyro_l );

} // end accelgyro_getMotion6Raw


/* Verify the I2C connection
 * Make sure that the device is connected and responds as expected.
 * @return True if connection is valid, false otherwise.
 */
int8_t accelgyro_testConnection()
{
	int8_t id = accelgyro_read( MPU6050_WHO_AM_I, 1 )[0];
	return id;

} // end accelgyro_testConnection()

void accelgyro_write( unsigned char registerAddress, unsigned char value )
{
	// TODO: NOTE that the I2C_STAT showed an error when there was none.
	I2C_STATUS status;

	// Initiate communication transaction with MPU6050 at I2C address
	status = I2C_MSTR_start( 0x68, I2C_MODE_MT );

	if (status != I2C_STAT_OK)
		UART0_printf( "Error with start write [ 0x%X ].\n", status );

	// Send register address
	status = I2C_MSTR_send( registerAddress );
	if (status != I2C_STAT_OK)
		UART0_printf( "Error with write addr [ 0x%X ].\n", status );

	// Send value to register address
	status = I2C_MSTR_send( value );
	if (status != I2C_STAT_OK)
		UART0_printf( "Error with write val [ 0x%X ].\n", status );

	// Complete transaction.
	I2C_MSTR_stop();

} // end accelgyro_write()

int8_t* accelgyro_read( unsigned char registerAddress, int numBytes )
{
	I2C_STATUS status;

	/* Buffer to store incoming I2C data */
	int8_t _buffer[ numBytes ];

	// Master Transmit --> transmit address where we will start reading from
	status = I2C_MSTR_start( MPU6050_ADDR, I2C_MODE_MT );

	if (status != I2C_STAT_OK)
		UART0_printf( "Error with start read [ 0x%X ].\n", status );


	// Send address from which we'll start multi-byte read
	status = I2C_MSTR_send( registerAddress );

	if (status != I2C_STAT_OK)
		UART0_printf( "Error with read addr [ 0x%X ].\n", status );

	if ( I2C_MSTR_start( MPU6050_ADDR, I2C_MODE_MR ) == I2C_STAT_OK )
	{
		I2C_MSTR_get_multiple( _buffer, numBytes );
	}
	else
		UART0_printf( "Error with read [ 0x%X ].\n", status );

	// end transaction
	I2C_MSTR_stop();

	return _buffer;

} // end accelgyro_read()



#endif

